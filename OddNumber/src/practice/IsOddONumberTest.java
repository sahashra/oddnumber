package practice;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class IsOddONumberTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsOdd() {
		assertTrue("Invalid value for Odd number", IsOddNumber.isOdd(5));
	}
	@Test
	public void testIsOddNegative() {
		assertFalse("Invalid value for negative result", IsOddNumber.isOdd(6));
	}
	@Test
	public void testIsOddBoundaryIn() {
		assertTrue("Invalid value for Odd number", IsOddNumber.isOdd(1));
	}
	@Test
	public void testIsOddBoundaryOut() {
		assertFalse("Invalid value for negative result", IsOddNumber.isOdd(2));
	}

}
