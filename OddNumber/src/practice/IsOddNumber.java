package practice;

public class IsOddNumber {

	public static boolean isOdd(int num) {
		if(num % 2 == 0)
			return false;
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Is 34 a odd number? " + isOdd(34));
		System.out.println("Is 35 a odd number? " + isOdd(35));
		System.out.println("Is 36 a odd number? " + isOdd(36));
	}

}
